<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Тернарные операторы</title>
</head>
<body>
    <?php
        // $a = 5;
        // $b = 4;
        // echo $a > $b ? 'ok' : 'no'

        $a = 148;
        $b = '51';
        $c = $a % $b;
        echo $c > 0 ? var_dump($c) : $a.'/'.$b.'='.$a/$b;
    ?>
</body>
</html>