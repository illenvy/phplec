<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dynamic</title>
</head>
<body>
    <?php
        $a = 2;
        $b = 2.0;
        $c = '2';
        $d = 'two';
        $g = true;
        $f = false;

        $a = (string)$a;
        echo gettype($a);

        $b = (string)$b;
        echo gettype($b);

        $c = (string)$c;
        echo gettype($c);

        $d = (string)$d;
        echo gettype($d);

        $g = (string)$g;
        echo gettype($g);

        $f = (string)$f;
        echo gettype($f);
    ?>
</body>
</html>