<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        $c = -27;
        $b = 12;
        if ($c > 0 and $b > 0)
            echo pow($c, $b);
        elseif ($c < 0 and $b < 0)
            echo $c + $b;
        else
            echo $c * $b;
    ?>
</body>
</html>