<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        // $apple = 12;
        // $format = "У Даши %010d яблок";
        // echo sprintf($format, $apple);

        $dateMech = date("h:i");
        $dateElec = date("Y-m-d h:i:s");
        $clockMech = 'Механические часы';
        $clockElec = 'Электронные часы';

        $format = "%s: %s";
        echo sprintf($format, $clockMech, $dateMech). "<br>";
        echo sprintf($format, $clockElec, $dateElec);
    ?>
</body>
</html>