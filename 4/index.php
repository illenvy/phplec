<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Конкатенация</title>
</head>
<body>
    <?php
        // $apple = 12;
        // $b = 5;
        // echo (pow($b, $apple)) // pow - возведение в степень

        $hunter = 'охотник';
        $wants = 'желает';
        $know = 'знать';
        $fizan = 'фазан';
        $sits = 'сидит';

        echo 'Каждый '.$hunter.' '.$wants.' '.$know.' где'. "<br>";
        echo "Каждый $hunter $wants $know где $sits $fizan". "<br>";

        $quieter = "Тише";
        $go = "едешь";
        $further = "дальше";

        echo "$quieter $go $further будешь";
    ?>
</body>
</html>