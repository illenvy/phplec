<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        $i = 3;
        $s = 0;
        $count = 0;
        while ($count < 15){
            $count++;
            $s += $i;
            $i += 3;
        }

        echo $s . "<br>";
        $s = 0;
        $count = 0;
        $i = 3;
        do {
            $count++;
            $s += $i;
            $i += 3;
        } while ($count < 15);

        echo $s;
    ?>
</body>
</html>